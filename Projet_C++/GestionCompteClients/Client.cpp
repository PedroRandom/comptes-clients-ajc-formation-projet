#include "Client.h"

Client::Client(string n, string pren, char sex, long long int telephone, string libAP, string compAP, string vil, int code)
{
    //ctor
    Setnom(n);
    Setprenom(pren);
    Setsexe(sex);
    Settelephone(telephone);
    SetAPost(new AdressePost(libAP,compAP,vil,code));
}

void Client::Setnom(string val)
{
    if(val.length()>50)
    {
        throw InputException(ErrCode::ERR_NOM);
    }
    else
    {
        nom=val;
    }
}

void Client::Setprenom(string val)
{
    if(val.length()>50)
    {
        throw InputException(ErrCode::ERR_NOM);
    }
    else
    {
        prenom=val;
    }
}
void Client::Setsexe(char val)
{
    if(val!='F'&&val!='M')
    {
        throw InputException(ErrCode::ERR_SEX);
    }
    else
    {
        sexe = val;
    }
}


Client::~Client()
{
    //dtor
    if (delete_comment)
    {
        cout<<"Client detruit"<<endl;
    }
    for(int i=0;i<lstCompts.size();i++)
    {
        if (delete_comment)
        {
            cout<<"Destruction du element"<<i<<endl;
        }
        delete lstCompts.at(i);
    }
    delete this->GetAPost();
}

AdressePost::AdressePost(string lib, string comp, string vil, int code)
:  libelle(lib),complement(comp),ville(vil),cp(code)
{
}

void Client::afficher()
{
    cout<< "Les infos du client sont : "<<endl;
    cout<<"\t Nom : "<<this->Getnom()<<endl;
    cout<<"\t Prenom: "<<this->Getprenom()<<endl;
    cout<<"\t Sexe : "<<this->Getsexe()<<endl;
    cout<<"\t Telephone : "<<this->Gettelephone()<<endl;
    cout<<"\t Adresse Postale : "<<endl;
    this->GetAPost()->afficher();
    cout<<endl;
}
void AdressePost::afficher()
{
    cout<<"\t\t Libelle: "<<this->libelle<<endl;
    cout<< "\t\t Complement: "<<this->complement<<endl;
    cout<<"\t\t Code Postal: "<<this->cp<<endl;
    cout<<"\t\t Ville : "<<this->ville<<endl;
}

void Client::afNom()
{
    cout<<this->Getprenom()<<" "<<this->Getnom()<<endl;
}
Dates::Dates(int j, int m, int a)
{
    Setjour(j);
    Setmois(m);
    Setanne(a);
}
Dates::~Dates()
{
    if (delete_comment)
    {
        cout<<"The date have been removed "<<endl;
    }
}

void Dates::Setjour(int j)
{
    if(j<=0||j>31)
    {
        throw InputException(ErrCode::ERR_JOUR);
    }
    else {this->jour=j;}
}

void Dates::Setmois(int m)
{
    if(m<=0||m>12)
    {
        throw InputException(ErrCode::ERR_MOIS);
    }
    else {this->mois=m;}
}

void Dates::Setanne(int a)
{
    if(a<1000)
    {
        throw InputException(ErrCode::ERR_ANNE);
    }
    else {this->anne=a;}
}
void Dates::afLigne()
{
    cout<<setw(2)<<this->Getjour()<<"/"<<setw(2)<<this->Getmois()<<"/"<<this->Getanne();
}
void Dates::afficher()
{
    this->afLigne();
    cout<<endl;
}

CompteBancaire::CompteBancaire(int nCompt, int j, int m, int a, float sold, float decouv)
{
    setnum(nCompt);
    Setouverture(new Dates(j,m,a));
    Setdecouvert(decouv);
    Setsolde(sold);
}

CompteBancaire::~CompteBancaire()
{
    if (delete_comment)
    {
        cout<<"Destruction du compte bancaire "<<endl;
    }
        for(int i=0;i<lstOps.size();i++)
    {
        if (delete_comment)
        {
            cout<<"Destruction du element"<<i<<endl;
        }
        delete lstOps.at(i);
    }
    delete this->Getouverture();
}
float CompteBancaire::Setdecouvert(float val)
{
    if (val<0)
    {
        throw InputException(ErrCode::ERR_DEC);
    }
    else
    {
        this->decouvert=val;
    }
}

void CompteBancaire::Setsolde(float val)
{
    if (val>=-this->Getdecouvert())
    {
        this->solde=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_SOLDE);
    }
}

void CompteBancaire::retraitSolde(float val)
{
    if (this->Getsolde()-val<-this->Getdecouvert())
    {
        throw InputException(ErrCode::ERR_SOLDE);
    }
    else
    {
        this->solde-=val;
    }
}

void CompteBancaire::addSolde(float val)
{
    if (val<0)
    {
        throw InputException(ErrCode::ERR_NUM,"montant d'argent a depositer");
    }
    else
    {
        this->solde+=val;
    }
}
void CompteBancaire::setnum(int n)
{
    if(n<0)
    {
        throw InputException(ErrCode::ERR_NUM,"numero de compte bancaire");
    }
    else
    {
        this->num=n;
    }
}

void CompteBancaire::afficher()
{
    cout<<"\t Les infos du compte numero "<<this->Getnum()<<" sont : "<<endl;
    cout<<"\t Date d'ouverture :";
    this->Getouverture()->afficher();
    cout<<"\t Solde : "<<this->Getsolde()<<endl;
    cout<<"\t Montant du d�couvert autoris� : "<<this->Getdecouvert()<<endl;
}
void CompteBancaire::afficherSolde()
{
    cout<<" Solde du compte "<<this->Getnum()<<" : "<<this->Getsolde()<<endl;
}

Operation::Operation(int j, int m, int a, int code, float montant)
{
    Setrealise(new Dates(j,m,a));
    Setcode(code);
    Setmontant(montant);
}

Operation::~Operation()
{
    if(delete_comment)
    {
        cout<<"Destruction d'operation"<<endl;
    }
    delete this->Getrealise();
}

void Operation::Setcode(int val)
{
    if (val==1||val==2||val==3)
    {
        this->code=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_OP_CODE);
    }
}

void Operation::Setmontant(float val)
{
    if (val>0)
    {
        this->montant=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_NUM,"montant");
    }
}
void Operation::afficher()
{
    cout<<"|\t ";
    this->Getrealise()->afLigne();
    cout<<"|\t "<<this->Getcode()<<"| \t "<<this->Getmontant()<<endl;
}
