#include "Particulaires.h"
#include "Professionnels.h"
#include "InputException.h"
#include "DataClients.h"
#define MAX_ESSAYS 5
void test(DataClients& a1)
{
    //DataClients al1Data;
    bool bingo=false;
    char c;
    float n;
    while(!bingo)
    {
    try
    {
        //cout<<"Introduire situation familiale"<<endl;
        //cin>>c;
        //Particulaires a('M',17,04,1993,"Nom","Prenom",'M',5783257693,"71 zezez0","","Toulouse",46701);
        //Professionnels b(12345678912345,"Raison sociale",2020,
         //                "Lib ent","Comp Ent","Toulouse Ent",46701,
         //                "mail@gmail.com",
         //                "Nom","Prenom",'M',5783257693,"71 zezez0","","Toulouse",46701);
        //a.infos();
        a1.addClient(new Particulaires('M',17,04,1993,"NomPart","Prenom",'M',5783257693,"71 zezez0","","Toulouse",46701));
        a1.addClient(new Professionnels(12345678912345,"Raison sociale",2020,
                         "Lib ent","Comp Ent","Toulouse Ent",46701,
                         "mail@gmail.com",
                         "NomProf","Prenom",'M',5783257693,"71 zezez0","","Toulouse",46701));
        //a1.afficheSimp();
        //a1.affiche();
        //al1Data.sup(1);
        //a1.afficheSimp();
        //b.infos();
        a1.GetlstClients()[0]->addCompt(new CompteBancaire(12,10,04,1995,15,3));
        a1.GetlstClients()[0]->addCompt(new CompteBancaire(15,12,05,1993,5,0));
        a1.GetlstClients()[1]->addCompt(new CompteBancaire(1,1,1,1991,1,1));
        a1.GetlstClients()[1]->addCompt(new CompteBancaire(2,2,2,1992,2,2));
        //b.afficheCompts();
        //CompteBancaire c(12,10,04,1993,5,0);
        //c.afficher();

        a1.GetlstClients()[0]->GetlstCompts()[1]->addOp(new Operation(17,04,1993,1,5));
        a1.GetlstClients()[0]->GetlstCompts()[1]->addOp(new Operation(18,05,1983,2,3));
        /*b.GetlstCompts()[0]->afficheOps();
        b.GetlstCompts()[1]->afficheOps();*/
        //c.afficheOps();
        //Operation p(17,04,1993,1,-5);
        //p.afficher();
        //cout<<endl;
        bingo=true;
    }
    catch(InputException &excpt)
    {
        cout<<"Probleme avec un des inputs :"<<endl;
        cout<<excpt.Getmessage()<<endl;
    }
    }
}
int menu();
void listAll(DataClients& dataBanque); // Eviter destruction pointers / Creation de moins de copies
void listComptes(DataClients& dataBanque);// Eviter destruction pointers / Creation de moins de copies
void gestionClient(DataClients& dataBanque);
void ajClient(DataClients& dataBanque,int etape,int pos,bool modif);
void affOperations(DataClients& dataBanque);
int validOption(list<int>);
void gestionOperation(DataClients& dataBanque);
void gestionCompt(CompteBancaire* compteBanc);
int main()
{
    int opt =-1,cpt=0;
    DataClients dataBanque;
    test(dataBanque);
    while (opt!=7)
    {
        opt=menu();
        switch (opt)
        {
        case 1:
            listAll(dataBanque);
            break;
        case 2:
            listComptes(dataBanque);
            break;
        case 3:
            gestionClient(dataBanque);
            break;
        case 4:
            gestionOperation(dataBanque);
            break;
        case 5:
            affOperations(dataBanque);
            break;
        case 6:
        case 7:
            break;
        default:
            cout<<"L'option saisi n'existe pas, le menu sera relance"<<endl;
            cpt++; // Compteur pour eviter boucle infini
            if (cpt>MAX_ESSAYS)
            {
                cout<<"Nombre maxim d'essayes attendu"<<endl;
                cout<<"L'application s'arret"<<endl;
                opt=7; // Option de sortie
            }
            break;
        }
    system("pause"); // Pour pouvoir lire l'output de l'option
    }
    return 0;
}
int menu()
{
    int opt =-1,n_opts=1;
    cout<<"Bonjour, bienvenu a l'application de gestion de comptes clients"<<endl;
    cout<<"Les options sont les suivants"<<endl;
    cout<<"\t Opt 1 : Lister l'ensemble de clients"<<endl;
    cout<<"\t Opt 2 : Consulter les soldes des comptes pour un client donn�"<<endl;
    cout<<"\t Opt 3 : Ajouter/Supprimer/Modifier un Client"<<endl;
    cout<<"\t Opt 4 : Ajouter/Supprimer/Modifier une Operation"<<endl;
    cout<<"\t Opt 5 : Afficher l'ensemble des operations pour un compte donn�"<<endl;
    cout<<"\t Opt 6 : Importer le fichier des Op�rations Bancaires"<<endl;
    cout<<"\t Opt 7 : Sortir du programme"<<endl;
    cout<<"Introduisez la option desire(1,7)"<<endl;//(1,2,3,4,5,6,7)"<<endl;
    cin>>opt;
    fflush(stdin); // Eviter problems avec les autres cin
    return opt;
}
void listAll(DataClients& dataBanque)
{
    int opt=-1,cpt=0;
    while (opt!=1&&opt!=2&&opt!=3)
    {
        cout<<"Affichage simple (Opt 1), Affichage Complet (Opt 2) ou Retourner au menu (Opt 3)"<<endl;
        cin>>opt;
        switch (opt)
        {
        case 1:
            dataBanque.afficheSimp();
            break;
        case 2:
            dataBanque.affiche();
            break;
        case 3:
            break;
        default:
            cout<<"L'option saisi n'existe pas,choisi une des suivants options"<<endl;
            cpt++; // Compteur pour eviter boucle infini
            if (cpt>MAX_ESSAYS)
            {
                cout<<"Nombre maxim d'essayes attendu"<<endl;
                cout<<"Le menu sera relance"<<endl;
                opt=3; // Option de sortie
            }
            break;
        }
    }
}
void listComptes(DataClients& dataBanque)
{
    int num=-1,cpt=0;
    bool sortir=false;
    while(!sortir)
    {
    try
    {
        cpt++;
        if (cpt-1>MAX_ESSAYS)
        {
            sortir=true;
        }
        cout<<"Introduissez le numero du client a consulter "<<endl;
        cin>>num;
        dataBanque[num]->afficheCompts();
        sortir=true;
    }
    catch(InputException &excpt)
    {
        cout<<"Probleme avec un des inputs :"<<endl;
        cout<<excpt.Getmessage()<<endl;
    }
    }
}
void gestionClient(DataClients& dataBanque)
{
    int opt=-1,cpt=0,etape=0,pos=0;
    list<int>etapeValides = {1,2,3,4,5,6,7,8,9,10,11,12,13,14};
    while (opt!=1&&opt!=2&&opt!=3&&opt!=4)
    {
        cout<<"Ajouter un client (Opt 1), Moedifier un client (Opt 2), Eliminer un client (Opt 3) ou Retourner au menu (Opt 4)"<<endl;
        cin>>opt;
        switch (opt)
        {
        case 1:
            ajClient(dataBanque,0,0,false);
            break;
        case 2:
            cout<<"Introduissez le client a modifier"<<endl;
            try
            {
                cin>>pos;
                cout<<"Cette numero correspond a Mr"<<dataBanque[pos]->Getnom()<<endl;
                cout<<"Introduissez le champ a modifier"<<endl;
                cout<<"1 Nom, 2 Prenom, 3 Sexe, 4 Telephone, 5 Libelle Adresse Postale "<<endl;
                cout<<"6 Complement Adresse, 7 Code Postal, 8 Ville"<<endl;
                cout<<"9 Situation familiale (Particulaire) ou Siret(Prof)"<<endl;
                cout<<"10 Jour de naissance (Particulaire) ou Raison sociale(Prof)"<<endl;
                cout<<"11 Mois de naissance (Particulaire) ou Ann�e de creation(Prof)"<<endl;
                cout<<"12 Anne de naissance (Particulaire) ou Adresse de l'enterprise (Prof)"<<endl;
                cout<<"13 Mail (Professionale)"<<endl;
                etape=validOption(etapeValides);
            }
            catch(InputException &excpt)
            {
                cout<<"Probleme avec un des inputs :"<<endl;
                cout<<excpt.Getmessage()<<endl;
            }
            ajClient(dataBanque,etape,pos,true);
            break;
        case 3:
            cout<<"Introduissez le client a eliminer"<<endl;
            try
            {
                cin>>pos;
                cout<<"Cette numero correspond a Mr"<<dataBanque[pos]->Getnom()<<endl;
                dataBanque.sup(pos);
            }
            catch(InputException &excpt)
            {
                cout<<"Probleme avec un des inputs :"<<endl;
                cout<<excpt.Getmessage()<<endl;
            }
            break;
        case 4:
            break;
        default:
            cout<<"L'option saisi n'existe pas, le menu sera relance"<<endl;
            cpt++; // Compteur pour eviter boucle infini
            if (cpt>MAX_ESSAYS)
            {
                cout<<"Nombre maxim d'essayes attendu"<<endl;
                cout<<"Le menu sera relance"<<endl;
                opt=3; // Option de sortie
            }
            break;
        }
    }
}
void ajClient(DataClients& dataBanque,int etape,int pos,bool modif)
{
    int typeClient=-1;
    string nom;
    nom=" ";
    char sexe='-';
    char situ='-';
    long long int telephone = 0;
    long long int siret=0;
    int numero=0;
    list<int>optionList={0,1};
    Particulaires * pPart = nullptr;
    Professionnels *pProf = nullptr;
    if (modif)
    {
        pPart = dynamic_cast<Particulaires *>(dataBanque[pos]);
        pProf = dynamic_cast<Professionnels *>(dataBanque[pos]);
    }
    //bool fields[10];
    //for_each(begin(fields),end(fields),[](bool& b){b=false;});
    while(etape<14)//(any_of(fields.begin(),fiels.end(),[](bool b){return b==false;}))
    {
        try
        {
            switch (etape)
            {
                //S'il a plus de temps reorganiser les case dans functions (input string,input int) pour eviter de se repeter
            case 0 :
                cout<<"Est un client particulaire(0) ou un client profesionel(1)"<<endl;
                typeClient=validOption(optionList); // optionList={0,1}
                if (typeClient==1)
                {
                     dataBanque.addClient(new Professionnels());
                }
                else
                {
                     dataBanque.addClient(new Particulaires());
                }

                pos=dataBanque.nbClients()-1;// Index = Nb-1
                pPart = dynamic_cast<Particulaires *>(dataBanque[pos]);
                pProf = dynamic_cast<Professionnels *>(dataBanque[pos]);
                etape++;
            case 1 :
                cout<<"Introduisez le nom (Max 50 char)"<<endl;
                cin>>nom;
                fflush(stdin);
                dataBanque[pos]->Setnom(nom);
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++; // Pasera a la suivant etape s'il n'y a pas d'erreur dans setnom
            case 2 :
                cout<<"Introduisez le prenom (Max 50 char)"<<endl;
                cin>>nom;
                fflush(stdin);
                dataBanque[pos]->Setprenom(nom);
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 3 :
                cout<<"Introduisez le sexe (F/M)"<<endl;
                cin>>sexe;
                fflush(stdin);
                dataBanque[pos]->Setsexe(sexe);
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 4 :
                cout<<"Introduisez le telephone"<<endl;
                cin>>telephone;
                fflush(stdin);
                dataBanque[pos]->Settelephone(telephone);
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 5 :
                cout<<"Introduisez la libelle de l'adresse postale"<<endl;
                cin>>nom; // Si plus de temps , verifier/regarder comment gerer les spaces dans cinn
                fflush(stdin);
                dataBanque[pos]->GetAPost()->libelle=nom;
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 6 :
                cout<<"Introduissez le complement de l'adresse postale"<<endl;
                cin>>nom;
                fflush(stdin);
                dataBanque[pos]->GetAPost()->complement=nom;
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 7 :
                cout<<"Introduissez le code postale de l'adresse postale"<<endl;
                cin>>numero;
                fflush(stdin);
                dataBanque[pos]->GetAPost()->cp=numero;
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 8 :
                cout<<"Introduissez la ville "<<endl;
                cin>>nom;
                fflush(stdin);
                dataBanque[pos]->GetAPost()->ville=nom;
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 9 :
                if (pPart!=nullptr)
                {

                    cout<<"Introduissez la situation familiale"<<endl;
                    cin>>situ;
                    fflush(stdin);
                    pPart->SetsituFam(situ);
                }
                else if(pProf!=nullptr)
                {
                    cout<<"Introduissez le siret (14 chiffres)"<<endl;
                    cin>>siret;
                    fflush(stdin);
                    pProf->Setsiret(siret);
                }
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 10 :
                if (pPart!=nullptr)
                {
                    cout<<"Introduissez le jour de naissance "<<endl;
                    cin>>numero;
                    fflush(stdin);
                    pPart->Getnaissance()->Setjour(numero);
                }
                else if(pProf!=nullptr)
                {
                    cout<<"Introduissez la raison sociale "<<endl;
                    cin>>nom;
                    fflush(stdin);
                    pProf->SetraisonSoc(nom);
                }
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 11 :
                if (pPart!=nullptr)
                {
                    cout<<"Introduissez le mois de naissance "<<endl;
                    cin>>numero;
                    fflush(stdin);
                    pPart->Getnaissance()->Setmois(numero);
                }
                else if(pProf!=nullptr)
                {
                    cout<<"Introduissez l'anne de creation"<<endl;
                    cin>>numero;
                    fflush(stdin);
                    pProf->Setcreation(numero);
                }
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 12 :
                if(pPart!=nullptr)
                {
                    cout<<"Introduissez l'anne de naissance"<<endl;
                    cin>>numero;
                    fflush(stdin);
                    pPart->Getnaissance()->Setanne(numero);
                }
                else if(pProf!=nullptr)
                {
                    cout<<"Introduissez la libelle de l'adresse de l'enterprise "<<endl;
                    cin>>nom;
                    fflush(stdin);
                    pProf->GetadresseEnt()->libelle=nom;
                    // Pour utiliser moins de temps, introduction de toute l'adresse dans le meme case parce qu'il n'y a pas de verifications
                    cout<<"Introduissez le complement de l'adresse de l'enterprise "<<endl;
                    cin>>nom;
                    fflush(stdin);
                    pProf->GetadresseEnt()->complement=nom;
                    cout<<"Introduissez le code postale de l'adresse de l'enterprise "<<endl;
                    cin>>numero;
                    fflush(stdin);
                    pProf->GetadresseEnt()->cp=numero;
                    cout<<"Introduissez la ville de l'adresse de l'enterprise"<<endl;
                    cin>>nom;
                    fflush(stdin);
                    pProf->GetadresseEnt()->ville=nom;
                }
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
            case 13 :
                if(pProf!=nullptr)
                {
                    cout<<"Introduissez le mail de l'enterprise (doit contenir @) "<<endl;
                    cin>>nom;
                    fflush(stdin);
                    pProf->Setmail(nom);
                }
                if (modif){etape=14;break;}// Parce que on veut que modifier 1 field
                etape++;
                break;
            }
        }
        catch(InputException &excpt)
        {
            cout<<"Probleme avec un des inputs :"<<endl;
            cout<<excpt.Getmessage()<<endl;
        }
    }
}
int validOption(list<int> optionList)
{
    int option=-1,cpt=0;
    cin>>option;
    while (!any_of(optionList.begin(),optionList.end(),[option](int j){return option==j;}))
    {
        cout<<"La option introduit n'est pas correct"<<endl;
        cout<<"Options disponibles :";
        for_each(optionList.begin(),end(optionList),[](int i){cout<<i<<" , ";});
        cout<<endl;
        cin>>option;
        /*if (cpt>MAX_ESSAYS) // Si plus de temps
        {
            option=-1;
            cout<<"Nombre maxim d'essayes attendu"<<endl;
            cout<<"Le menu sera relance"<<endl;
            break;
        }*/
    }
    return option;
}
void affOperations(DataClients& dataBanque)
{
    int pos,compt;
    pos=0,compt=0;
    cout<<"Introduissez le client a consulter"<<endl;
    try
    {
        cin>>pos;
        cout<<"Cette numero correspond a Mr"<<dataBanque[pos]->Getnom()<<endl;
        cout<<"Introduissez le compte a consulter"<<endl;
        cin>>compt;
        dataBanque[pos]->getCompt(compt)->afficheOps();
    }
    catch(InputException &excpt)
    {
        cout<<"Probleme avec un des inputs :"<<endl;
        cout<<excpt.Getmessage()<<endl;
    }
}
void gestionOperation(DataClients& dataBanque)
{ // Si plus de temps il faudrait mettre la part initiale dans une fonction
    int pos,compt;
    pos=0,compt=0;
    CompteBancaire* compteBanc=nullptr;
    cout<<"Introduissez le client a consulter"<<endl;
    try
    {
        cin>>pos;
        cout<<"Cette numero correspond a Mr"<<dataBanque[pos]->Getnom()<<endl;
        cout<<"Introduissez le compte a modifier"<<endl;
        cin>>compt;
        compteBanc=dataBanque[pos]->getCompt(compt);
        gestionCompt(compteBanc);
    }
    catch(InputException &excpt)
    {
        cout<<"Probleme avec un des inputs :"<<endl;
        cout<<excpt.Getmessage()<<endl;
    }
    compteBanc=nullptr;
    delete compteBanc;
}

void gestionCompt(CompteBancaire* compteBanc)
{
    int type=-1,pos=-1;
    list<int> optionList={0,1,2,3,4};
    bool finished=false;
    cout<<"Ajouter une nouvelle operation(0) suprimer une operation(1), modifier une operation(2), sortir (3)"<<endl;
    type=validOption(optionList);
    switch(type)
    {
    case 1:
        while(!finished)
        {
        try
        {
            cout<<"Introduissez la operation a eliminer"
            cin>>pos;
            compteBanc->sup(pos);
            finished=true;
        }
        catch(InputException &excpt)
        {
            cout<<"Probleme avec un des inputs :"<<endl;
            cout<<excpt.Getmessage()<<endl;
        }
        }
        break;
    case 0:
    case 2:
    case 3:
        break;

    }
}
