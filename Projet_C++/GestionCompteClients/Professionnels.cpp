#include "Professionnels.h"

Professionnels::Professionnels(long long int sir, string rSoc,int creation, string libEnt, string compEnt, string vilEnt, unsigned int codeEnt, string mail, string n, string pren, char sex, long long int telephone, string libAP, string compAP, string vil, int code)
:Client(n,pren,sex,telephone,libAP,compAP,vil,code)
{
    //ctor
    Setsiret(sir);
    SetraisonSoc(rSoc);
    Setcreation(creation);
    SetadresseEnt(new AdressePost(libEnt,compEnt,vilEnt,codeEnt));
    Setmail(mail);
}

Professionnels::~Professionnels()
{
    //dtor
    if (delete_comment)
    {
        cout<<"Destruction du professionel";
    }
    delete this->GetadresseEnt();
}
void Professionnels::Setsiret(long long int val)
{
    if (val<10000000000000||val>99999999999999)
    {
        throw InputException(ErrCode::ERR_SIRET);
    }
    else
    {
         siret = val;
    }
}

void Professionnels::SetraisonSoc(string val)
{
    if(val.length()>50)
    {
        throw InputException(ErrCode::ERR_NOM);
    }
    else
    {
        this->raisonSoc=val;
    }
}

void Professionnels::Setcreation(int a)
{
    if(a<1000)
    {
        throw InputException(ErrCode::ERR_ANNE);
    }
    else {this->creation=a;}
}

void Professionnels::Setmail(string val)
{
    if(val.find_first_of('@')!=val.npos)
    {
        this->mail=val;
    }
    else
    {
        throw InputException(ErrCode::ERR_MAIL);
    }
}

void Professionnels::infos()
{
    Client::afficher();
    cout<<"\t Siret : "<<this->Getsiret()<<endl;
    cout<<"\t Rasion Social : "<<this->GetraisonSoc()<<endl;
    cout<<"\t Ann�e de Creation : "<<this->Getcreation()<<endl;
    cout<<"\t Adresse postale de l'enterprise : "<<endl;
    this->GetadresseEnt()->afficher();
    cout<<"\t Mail : "<<this->mail<<endl;
}
