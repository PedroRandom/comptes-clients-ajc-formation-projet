#ifndef PROFESSIONNELS_H
#define PROFESSIONNELS_H

#include "Client.h"


class Professionnels : public Client
{
    public:
        Professionnels(long long int sir=10000000000000,string rSoc="A remplir",int creation=1000,
                      string libEnt="A reemplir", string compEnt="",string vilEnt="A reemplir",unsigned int codeEnt=0,string mail="areemplir@aremplir.com",
                      string n="A remplir",string pren="A remplir",char sex='M', long long int telephone=0,
                      string libAP="A reemplir", string compAP="",string vil="A reemplir", int code=0);
        virtual ~Professionnels();

        long long int Getsiret() { return siret; }
        void Setsiret(long long int val);
        string GetraisonSoc() { return raisonSoc; }
        void SetraisonSoc(string val);
        int Getcreation() { return creation; }
        void Setcreation(int a);
        AdressePost* GetadresseEnt() { return adresseEnt; }
        void SetadresseEnt(AdressePost* val) { adresseEnt = val; }
        string Getmail() { return mail; }
        void Setmail(string val);
        void infos() override;

    protected:

    private:
        long long int siret;
        string raisonSoc;
        int creation;
        AdressePost* adresseEnt;
        string mail;
};

#endif // PROFESSIONNELS_H
