#include "Particulaires.h"

Particulaires::Particulaires(char situFam, int jour, int mois, int anne, string n, string pren, char sex, long long int telephone, string libAP, string compAP, string vil, int code)
: Client(n,pren,sex,telephone,libAP,compAP,vil,code)
{
    //ctor
    SetsituFam(situFam);
    Setnaissance(new Dates(jour,mois,anne));
}

Particulaires::~Particulaires()
{
    //dtor
    if (delete_comment)
    {
        cout<<"Destruction du Particulaire"<<endl;
    }
    delete this->Getnaissance();
}
void Particulaires::SetsituFam(char situFam)
{
    switch (situFam)
    {
        case 'C':
            this->situFam=situationFamiliale::Celibataire;
            break;
        case 'M':
            this->situFam=situationFamiliale::Marie;
            break;
        case 'D':
            this->situFam=situationFamiliale::Divorce;
            break;
        case 'X':
            this->situFam=situationFamiliale::Autre;
            break;
        default:
            throw InputException(ErrCode::ERR_SITU_FAM);
    }
}
void Particulaires::infos()
{
    Client::afficher();
    cout<<"\tDate de naissance ";
    this->Getnaissance()->afficher();
    cout<<"\tSituation Familiale : ";
    switch (this->GetsituFam())
    {
        case situationFamiliale::Celibataire:
            cout<<"Celibataire"<<endl;
            break;
        case situationFamiliale::Marie:
            cout<<"Marie"<<endl;
            break;
        case situationFamiliale::Divorce:
            cout<<"Divorce"<<endl;
            break;
        case situationFamiliale::Autre:
            cout<<"Autre"<<endl;
            break;
        default:
            throw InputException(ErrCode::ERR_SITU_FAM);
    }
}

