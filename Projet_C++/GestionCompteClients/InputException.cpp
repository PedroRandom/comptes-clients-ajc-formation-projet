#include "InputException.h"

InputException::InputException(ErrCode err,string text,int maxi)
{
    this->code = err;
    this->text = text;
    this->maxi = maxi;
}

InputException::~InputException()
{
}
string InputException::Getmessage()
{
    ostringstream oss;
    switch(this->code)
    {
    case ErrCode::ERR_JOUR:
        oss<<"Le jour saissi doit �tre entre 1 et 31 ";
        break;
    case ErrCode::ERR_MOIS:
        oss<<"Le mois saissi doit �tre entre 1 et 12 ";
        break;
    case ErrCode::ERR_ANNE:
        oss<<"L'anne saissi doit �tre en fomat de 4 chifres (AAAA)";
        break;
    case ErrCode::ERR_NUM:
        oss<<"Le "<<this->text<<" saisi doit �tre positif";
        break;
    case ErrCode::ERR_NOM:
        oss<<"La maximum longeur pour les nom, prenoms et raison sociale est 50";
        break;
    case ErrCode::ERR_SEX:
        oss<<"La sexe doit etre soit M (Masculin) ou F (Femenine)";
        break;
    case ErrCode::ERR_SITU_FAM:
        oss<<"La situation familiale doit �tre 'C','M','D' ou 'X' (C�libataire,Mari�(e),Divorc�(e),Autre)";
        break;
    case ErrCode::ERR_MAIL:
        oss<<"Le mail doit contenir le character '@' ";
        break;
    case ErrCode::ERR_SIRET:
        oss<<"Le SIRET doit �tre composse de 14 chiffres";
        break;
    case ErrCode::ERR_SOLDE:
        oss<<"Le solde du compte doit etre superieur au montant du decouverte autorise ";
        break;
    case ErrCode::ERR_DEC:
        oss<<"Le montant du d�couvert autoris� doit etre superieur ou egal � 0";
        break;
    case ErrCode::ERR_OP_CODE:
        oss<<"Le code d'operation doit �tre 1,2 ou 3";
        break;
    case ErrCode::ERR_MAX:
        oss<<"L'index introduit doit etre inferieur ou egal a "<<this->maxi;
        break;
    default:
        oss<<"Autre erreur";
        break;
    }
    return oss.str();
}
