#ifndef CLIENT_H
#define CLIENT_H
#include "InputException.h"

class AdressePost
{
public:
    string libelle;
    string complement;
    string ville;
    int cp;
    void afficher();

    AdressePost(string lib="A reemplir",string comp="",string vil=" A reemplir",int code=0);;
    ~AdressePost(){
        if(delete_comment){cout<<"Destruction d'adresse postale "<<endl;}}
};

class Dates
{
public:
    Dates(int j=30,int m=2,int a=1000);
    ~Dates();
    int Getjour(){return jour;}
    int Getmois(){return mois;}
    int Getanne(){return anne;}
    void Setjour(int);
    void Setmois(int);
    void Setanne(int);
    void afLigne();
    virtual void afficher();
private:
    int jour;
    int mois;
    int anne;

};
class Operation
{
public:
    Operation(int j=30,int m=2,int a=1000,int code=3,float montant=0);
    ~Operation();
    Dates* Getrealise(){return realise;}
    void Setrealise(Dates* val){realise=val;}
    void Setcode(int val);
    int Getcode(){return code;}
    void Setmontant(float val);
    float Getmontant(){return montant;}
    void afficher();
private:
    Dates* realise;
    int code;
    float montant;
};

class CompteBancaire
{
public:
    CompteBancaire(int nCompt=0,int j=30,int m=2,int a=1000,float sold=0,float decouv=0);
    ~CompteBancaire();
    Dates* Getouverture() { return ouverture; }
    void setnum(int n);
    int Getnum(){return num;}
    void Setouverture(Dates* ouv) { ouverture = ouv; }
    float Getdecouvert(){return decouvert;}
    float Setdecouvert(float val);
    float Getsolde(){return solde;}
    void Setsolde(float);
    void retraitSolde(float);
    void addSolde(float);
    void afficher();
    void afficherSolde();

    vector<Operation*> GetlstOps() { return lstOps; }
    void Setlst(vector<Operation*> val) { lstOps = val; }
    void addOp(Operation* val){lstOps.push_back(val);};
    void sup(int val){
        if (val>=this->nbOp())
            {
                throw InputException(ErrCode::ERR_MAX," ",this->nbOp()-1);
            }
            else
            {
                delete lstOps.at(val);
                lstOps.erase(lstOps.begin()+val);
            }
        /*lstOps.erase(lstOps.begin()+val);*/}
    int nbOp(){return lstOps.size();}
    void afficheOps()
    {
        cout<<"Operations effectues dans le compte numero :"<<this->Getnum()<<endl;
        cout<<"\t Date      | Code | Montant "<<endl;
        for (auto val: this->GetlstOps())
        {
            val->afficher();
        }
     }
private:
    int num;
    Dates* ouverture;
    float decouvert;
    float solde;
    vector <Operation*> lstOps;
};


class Client
{
    public:
        Client(string n="A remplir",string pren="A remplir",char sex='-',long long int telephone=0,
               string libAP="A reemplir", string compAP="",string vil="A reemplir",int code=0);
        virtual ~Client();

        string Getnom() { return nom; }
        void Setnom(string val);
        string Getprenom() { return prenom; }
        void Setprenom(string val);
        char Getsexe() { return sexe; }
        void Setsexe(char val);
        long long int Gettelephone() { return telephone; }
        void Settelephone(unsigned long long int val) { telephone = val; }
        AdressePost* GetAPost() { return APost; }
        void SetAPost(AdressePost* val) { APost = val; }
        virtual void afficher();
        virtual void afNom();
        virtual void infos()=0;

        vector<CompteBancaire*> GetlstCompts() { return lstCompts; }
        void Setlst(vector<CompteBancaire*> val) { lstCompts = val; }
        void addCompt(CompteBancaire* val){lstCompts.push_back(val);};
        void sup(int val){
            delete lstCompts.at(val);
            lstCompts.erase(lstCompts.begin()+val);}
        int nbCompts(){return lstCompts.size();}
        void afficheCompts()
        {
            cout<<"Comptes a nom du Mr:"<<this->Getnom()<<endl;
            for (auto val: this->GetlstCompts())
            {
                val->afficherSolde();
            }
        }
        CompteBancaire* getCompt (int pos)
        {
            if (pos>=this->nbCompts())
            {
                throw InputException(ErrCode::ERR_MAX," ",this->nbCompts()-1);
            }
            else
            {
                return lstCompts.at(pos);
            }
        }

    protected:

    private:
        string nom;
        string prenom;
        char sexe;
        unsigned long long int telephone;
        AdressePost* APost;
        vector<CompteBancaire*> lstCompts;
};

#endif // CLIENT_H
