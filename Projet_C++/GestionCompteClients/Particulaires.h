#ifndef PARTICULAIRES_H
#define PARTICULAIRES_H

#include "Client.h"

enum situationFamiliale{Celibataire,Marie,Divorce,Autre};
class Particulaires : public Client
{
    public:
        Particulaires(char situFam='X',int jour=30,int mois=2,int anne=1000,
                      string n="A remplir",string pren="A remplir",char sex='M',long long int telephone=0,
                      string libAP="A reemplir", string compAP="",string vil="A reemplir", int code=0);
        virtual ~Particulaires();

        situationFamiliale GetsituFam() { return situFam; }
        void SetsituFam(char situFam);
        Dates* Getnaissance() { return naissance; }
        void Setnaissance(Dates* val) { naissance = val; }
        void infos() override;


    protected:

    private:
        situationFamiliale situFam;
        Dates* naissance;
};

#endif // PARTICULAIRES_H
