#ifndef DATACLIENTS_H
#define DATACLIENTS_H

#include "Client.h"

class DataClients
{
    public:
        DataClients();
        virtual ~DataClients();

        vector<Client*> GetlstClients() { return lstClients; }
        void SetlstClients(vector<Client*> val) { lstClients = val; }
        void addClient(Client* val){lstClients.push_back(val);};
        void sup(int val){
            delete lstClients.at(val);
            lstClients.erase(lstClients.begin()+val);}
        int nbClients(){return lstClients.size();}
        void affiche()
        {
            cout<<"La information complete de nos "<<this->nbClients()<<" clients est :"<<endl;
            for (auto val: this->GetlstClients())
            {
                val->infos();
            }
        }
        void afficheSimp()
        {
            cout<<"La liste de nos "<<this->nbClients()<<" clients est :"<<endl;
            for (auto val: this->GetlstClients())
            {
                cout<<"Mr "<<val->Getnom()<<endl;
            }
        }
        void rechByNom(string nom);
        Client* operator[] (int pos)
        {
            if (pos>=this->nbClients())
            {
                throw InputException(ErrCode::ERR_MAX," ",this->nbClients()-1);
            }
            else
            {
                return lstClients.at(pos);
            }
        }


    protected:

    private:
        vector<Client*> lstClients;
};

#endif // DATACLIENTS_H
