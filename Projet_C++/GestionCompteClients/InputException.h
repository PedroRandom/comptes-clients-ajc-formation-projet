#ifndef INPUTEXCEPTION_H
#define INPUTEXCEPTION_H

#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <list>
#include <array>
#include <iomanip>
#include <stdio.h>

using namespace std;
#define delete_comment 0

enum class ErrCode{ERR_JOUR, ERR_MOIS, ERR_ANNE,ERR_NUM,
            ERR_NOM,ERR_SEX,ERR_SITU_FAM,ERR_MAIL,ERR_SIRET,
            ERR_SOLDE,ERR_DEC,ERR_OP_CODE,ERR_MAX};

class InputException
{
    public:
        InputException(ErrCode,string text="numero",int maxi=0);
        virtual ~InputException();

        string Getmessage();

    protected:

    private:
        ErrCode code;
        string text;
        int maxi;
};

#endif // INPUTEXCEPTION_H
